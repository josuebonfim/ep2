# EP2 - Orientação à Objetos

**Aluno**: Josué Bonfim </br>
**MAtrícula**: 16/0032598

## Instruções de Compilação

O jogo foi totalmente desenvolvido na IDE Eclipe. Para que seja possível a complilação e execução da aplicação, é recomendado que se use o Eclipse. Não foi possível a elaboração de um `Makefile` para uso fora da IDE.

O programa foi elaborado utilizando apenas um `package` que é o `ep2`.

## Instruções para Jogar

Quando a aplicação é compilada, aparecerá uma tela com meu nome, matrícula e um botão escrito `NOVO JOGO`.

Ao clicar no botão, será mostrada uma nova janela com um texto pedindo para selecionar o tipo e Snake. Tem-se 3 tipos que são a `COMUM`, `KITTY` e `STAR`. 

Selecionando o tipo do Snake, será redirecionado para a Tela do Jogo. Ao morrer, aparecerá uma tela escrita: `GAME OVER` e também `PLACAR FINAL`.