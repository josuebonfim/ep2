package ep2;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;


public class Menu {
	
	private JFrame frame;
	
	Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	
	
	public Menu(){}
	
	public Menu(String title)
	{
		frame = new JFrame();
		frame.setTitle(title);
		frame.setBounds(screenSize.height/2, screenSize.width/2, 600, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel label = new JLabel("EP 2 - Snake Game");
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setBounds(frame.getSize().width/2 - 80, frame.getSize().height/2 - 100, 160, 40);
		frame.getContentPane().add(label);
		
		JLabel label1 = new JLabel("Josué Bonfim - 16/0032598");
		label1.setHorizontalAlignment(SwingConstants.CENTER);
		label1.setBounds(frame.getSize().width/2 - 150, frame.getSize().height/2 - 70, 300, 40);
		frame.getContentPane().add(label1);
		
		JButton play = new JButton("NOVO JOGO");
		play.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				new Painel2("Snake Game");
			}
		});
		play.setBounds(frame.getSize().width/2 - 75, frame.getSize().height/2 - 20, 150, 40);
		frame.getContentPane().add(play);
		
		frame.setLocationRelativeTo(null);
	}
	
	public static void main(String args[])
	{
		Menu menu = new Menu("Snake Game");
		menu.frame.setVisible(true);
	}

}

