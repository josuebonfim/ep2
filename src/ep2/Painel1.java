package ep2;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Random;
import java.util.function.ObjLongConsumer;

import javax.swing.*;




public class Painel1 extends JPanel implements Runnable, KeyListener{
	
	private static final long serialVersionUID = 1L;

	// Altura e Largura da tela
	private static final int WIDTH = 600, HEIGHT = 600;
	
	private String tipo;

	//Thread do Jogo
	private Thread thread;
	//Verifica se o jogo está rodando
	private boolean running;
	//Movimentação da cobra
	private boolean direita = true, esquerda = false, cima = false, baixo = false;
	
	//Corpo do Snake
	private Snake s;
	private ArrayList<Snake> snake;
	private int tamanho = 5;
	
	//Frutas
	private Fruta fruta;
	private ArrayList<Fruta> frutas;
	private ArrayList<Fruta> obstaculos;
	private Random random;
	
	private int xCoor = 10, yCoor = 10, ticks = 0, placar = 0;
	

	public Painel1(String tipo)
	{
		this.tipo = tipo;
		
		setFocusable(true);

		setPreferredSize(new Dimension(WIDTH, HEIGHT));
		addKeyListener(this);
		
		snake = new ArrayList<Snake>();
		frutas = new ArrayList<Fruta>();
		obstaculos = new ArrayList<Fruta>();
		
		random = new Random();
		
		start();
	}
	
	@Override
	public void run() {
		while(running) {
			tick();
			repaint();
		}
	}
	
	public void start()
	{
		if(running)
			return;
		running = true;
		thread = new Thread(this);
		thread.start();
	}
	
	public void stop() {
		if(!running)
			return;
		running = false;
		try {
			thread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public void tick() {
		if (snake.size() == 0) {
			s = new Snake(xCoor, yCoor, 10);
			snake.add(s);
		}
		
		ticks++;
		
		if (ticks > 250000) 
		{
			if (direita)
				xCoor++;
			if (esquerda)
				xCoor--;
			if (cima)
				yCoor--;
			if (baixo)
				yCoor++;

			ticks = 0;

			s = new Snake(xCoor, yCoor, 10);
			snake.add(s);
			if (snake.size() > tamanho) {
				snake.remove(0);
			}
		}
		if(frutas.size() == 0) {
			int xCoor = random.nextInt(49);
			int yCoor = random.nextInt(49);
			
			fruta = new Fruta(xCoor, yCoor, 10);
			frutas.add(fruta);
		}
		
		//Verifica se o Snake colidiu com a fruta
		for(int i = 0; i < frutas.size(); i++) {
			if(xCoor == frutas.get(i).getXCoor() && yCoor == frutas.get(i).getYCoor()) {
				if(frutas.get(i).getTipo() == "bomb") {
					if(tipo == "Star")
						new GameOver(placar * 2);
					else 
						new GameOver(placar);
				stop();
			}else if(frutas.get(i).getTipo() == "decrease") {
				tamanho = 5;
				while(snake.size()!=5) {
					snake.remove(0);
				}
				placar++;
			}else if(frutas.get(i).getTipo() == "big") {
				tamanho += 2;
				placar += 2;
			}else {
				placar++;
				tamanho ++;
			}
			if(tipo == "Comum" || tipo =="Kitty")
				this.placar += frutas.get(i).getPlacar();
			frutas.remove(i);
			i++;
			}
		}
		
		//Verifica colisão com a o corpo do Snake
		for(int i = 0; i < snake.size(); i++) {
			if(xCoor == snake.get(i).getXCoor() && yCoor == snake.get(i).getYCoor()){
				if(i != snake.size()-1) {
					if(tipo == "Star")
						new GameOver(placar * 2);
					else 
						new GameOver(placar);
					stop();
				}
			}
		}
		
		if(xCoor < 0) {
			xCoor = 49;
		}
		if(yCoor < 0) {
			yCoor = 49;
		}
		if(xCoor > 49) {
			xCoor = 0;
		}
		if(yCoor > 49) {
			yCoor = 0;
		}
	}
	
	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		int key = e.getKeyCode();
		if (key == KeyEvent.VK_RIGHT && !esquerda) {
			direita = true;
			cima = false;
			baixo = false;
		}
		if (key == KeyEvent.VK_LEFT && !direita) {
			esquerda = true;
			cima = false;
			baixo = false;
		}
		if (key == KeyEvent.VK_UP && !baixo) {
			cima = true;
			direita = false;
			esquerda = false;
		}
		if (key == KeyEvent.VK_DOWN && !cima) {
			direita = false;
			baixo = true;
			esquerda = false;
		}
		
	}



	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	public void paint(Graphics g) {
		g.clearRect(0, 0, WIDTH, HEIGHT);
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, WIDTH, HEIGHT);

		for (int i = 0; i < WIDTH / 10; i++) {
			g.drawLine(i * 10, 0, i * 10, HEIGHT);
		}
		for (int i = 0; i < WIDTH / 10; i++) {
			g.drawLine(0, i * 10, HEIGHT, i * 10);
		}
		
		for (int i = 0; i < snake.size(); i++) {
			snake.get(i).draw(g);
		}
		for(int i = 0; i < frutas.size(); i++) {
			frutas.get(i).draw(g);
		}
		
		for(int i = 10;i < 20; i++) {	
			fruta = new Fruta(15, i, 10, "obstaculo");
			obstaculos.add(fruta);
			fruta = new Fruta(i, 15, 10,"obstaculo");
			obstaculos.add(fruta);
		}
		for(int i = 20; i < 25; i++) {
			fruta = new Fruta(45, i, 10, "obstaculo");
			obstaculos.add(fruta);
			fruta = new Fruta(i, 45, 10, "obstaculo");
			obstaculos.add(fruta);
		}
		for(int i = 30; i < 35; i++) {
			fruta = new Fruta(30, i, 10, "obstaculo");
			obstaculos.add(fruta);
			fruta = new Fruta(i, 35, 10, "obstaculo");
			obstaculos.add(fruta);
		}
		
		for(int i = 0; i < obstaculos.size(); i++){
			obstaculos.get(i).draw(g);
		}
		for(int i = 0; i < obstaculos.size(); i++) {
			if(tipo == "Kitty") {
				break;
			}
			if(xCoor == obstaculos.get(i).getXCoor() && yCoor == obstaculos.get(i).getYCoor()) {
				if(tipo == "Star")
					new GameOver(placar * 2);
				else 
					new GameOver(placar);
				stop();
				break;
			}
		}
	}
}
