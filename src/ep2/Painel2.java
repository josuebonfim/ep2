package ep2;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Label;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;




public class Painel2 {
	
	private JFrame frame;
	
	Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	
	public Painel2() {
		
	}
	
	public Painel2(String title) {
		frame = new JFrame();
		frame.setTitle(title);
		frame.setBounds(screenSize.height/2, screenSize.width/2, 600, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel label = new JLabel("Selecione o tipo de Snake");
		label.setBounds(frame.getSize().width/2 - 80, frame.getSize().height/2 - 100, 160, 40);
		frame.getContentPane().add(label);
		
		//Cobra tipo comum
		JButton comum = new JButton("Comum");
		comum.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new Main("Comum");
			}
		});
		comum.setBounds(frame.getSize().width/2 - 50, frame.getSize().height/2 - 40, 100, 25);
		frame.getContentPane().add(comum);
		
		//Cobra tipo kitty
		JButton kitty = new JButton("Kitty");
		kitty.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new Main("Kitty");
			}
		});
		kitty.setBounds(frame.getSize().width/2 - 50, frame.getSize().height/2 - 0, 100, 25);
		frame.getContentPane().add(kitty);
		
		//Cobra tipo Star
		JButton star = new JButton("Star");
		star.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new Main("Star");
			}
		});
		star.setBounds(frame.getSize().width/2 - 50, frame.getSize().height/2 + 40, 100, 25);
		frame.getContentPane().add(star);
		frame.setVisible(true);
		frame.setLocationRelativeTo(null);
		
	}
	
	public static void main(String args[]) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Painel2 window = new Painel2();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
		});
	}

}
