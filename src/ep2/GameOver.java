package ep2;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;

public class GameOver {
	
	private JFrame frame;
	public static int placar;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GameOver fim = new GameOver(placar);
					fim.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public GameOver(int placar) {
		frame = new JFrame();
		frame.setTitle("Fim de Jogo");
		frame.setBounds(100, 100, 250, 250);
		frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		
		JLabel fim = new JLabel("GAME OVER!");
		fim.setBounds(70, 60, 100, 25);
		frame.getContentPane().add(fim);
		
		JLabel pontos = new JLabel("PLACAR FINAL: " + placar * 100);
		pontos.setBounds(70, 120, 170, 25);
		frame.getContentPane().add(pontos);

		
		frame.getContentPane().setLayout(null);
		frame.setVisible(true);
		frame.setLocationRelativeTo(null);
	}
}