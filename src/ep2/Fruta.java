package ep2;

import java.awt.Color;
import java.awt.Graphics;
import java.security.Timestamp;
import java.util.ArrayList;
import java.util.Random;

public class Fruta {
	
	private int xCoor, yCoor, width, height, placar;
	private String tipo;
	
	public Fruta() {}
	
	public Fruta(int xCoor, int yCoor, int tamanho) {
		
		this.xCoor = xCoor;
		this.yCoor = yCoor;
		
		width = tamanho;
		height = tamanho;
		
		ArrayList<String> tipos = new ArrayList<String>();
		tipos.add("simple");
		tipos.add("bomb");
		tipos.add("decrease");
		tipos.add("big");
		
		Random random = new Random();
		int tipo_fruta = random.nextInt(4);
		this.tipo = tipos.get(tipo_fruta);
		
		if(tipo == "simples")
			this.placar = 1;
		else if(tipo == "big")
			this.placar = 2;
		else if(tipo == "bomb")
			this.placar = 0;
		else if(tipo == "obstaculo")
			this.placar = 0;	
	}
	
	public Fruta(int xCoor, int yCoor, int tileSize, String tipo) {
		this.xCoor = xCoor;
		this.yCoor = yCoor;
		width = tileSize;
		height = tileSize;
		
		this.tipo = tipo;
		
		if(tipo == "simples") {
			this.placar = 1;
		}else if(tipo == "big") {
			this.placar = 2;
		}else if(tipo == "decrease") {
			this.placar = 0;
		}else if(tipo == "bomb") {
			this.placar = 0;
		}else if(tipo == "obstaculo") {
			this.placar = 0;
		}
		
		
	}
	
	public int getXCoor() {
		return xCoor;
	}

	public void setXCoor(int xCoor) {
		this.xCoor = xCoor;
	}

	public int getYCoor() {
		return yCoor;
	}

	public void setYCoor(int yCoor) {
		this.yCoor = yCoor;
	}
	
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	public int getPlacar() {
		return placar;
	}
	
	public void setPlacar(int placar) {
		this.placar = placar;
	}
	
	public void draw(Graphics g) {
		if(tipo == "simples")
			g.setColor(Color.yellow);
		else if(tipo == "big")
			g.setColor(Color.BLUE);
		else if(tipo == "bomb")
			g.setColor(Color.RED);
		else if(tipo == "decrease")
			g.setColor(Color.orange);
		else if(tipo == "obstaculo")
			g.setColor(Color.GRAY);	
		g.fillRect(xCoor * width, yCoor * height, width, height);
	}

}
