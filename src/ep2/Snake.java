package ep2;

import java.awt.Color;
import java.awt.Graphics;

public class Snake {
	
	private int xCoor, yCoor, width, height;
	
	public String tipo;
	
	public Snake() {}
	
	public Snake(int xCoor, int yCoor, int tamanho)
	{
		this.yCoor = yCoor;
		this.xCoor = xCoor;
		
		width = tamanho;
		height = tamanho;
	}
	
	public int getXCoor() {
		return xCoor;
	}
	
	public void setXCoor(int xCoor) {
		this.xCoor = xCoor;
	}
	
	public int getYCoor() {
		return yCoor;
	}
	
	public void setYCoor(int yCoor) {
		this.yCoor = yCoor;
	}

	public String getTipo() {
		return tipo;
	}
	
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	public void draw(Graphics g)
	{
		g.setColor(Color.green);
		g.fillRect(xCoor * width, yCoor * height, width, height);
	}
}
